{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Lesson 3 - Simple Robotic Intelligence\n",
    "\n",
    "In the previous lesson we focussed mainly on the sensors that can be attached to our robot Rob. However, the main purpose of adding sensors is to allow the robot to make *smart* decisions, reacting to what is happening around the robot.\n",
    "\n",
    "Let's now take a closer look at how to make our robot Rob smarter. In this lesson we will work on three different situations:\n",
    "* Approaches on how to let Rob avoid the wall\n",
    "* Approaches on how to check if Rob fits through a hole in the wall\n",
    "* First step in making Rob autonomous\n",
    "\n",
    "## Avoiding walls\n",
    "\n",
    "As indicated, this lesson starts with different approaches for keeping Rob from hitting the wall using sensors.\n",
    "We start with using the sonar sensors as used in Lesson 2.\n",
    "This module starts with a quick recapitulation of what we learned about Rob and the sonar sensors in Module 2.\n",
    "\n",
    "By running the code block below, we again define our robot Rob, in his 4x4m space. This time, Rob is equipped with the 16 sonar sensors from the start. Look at *Figure 1* to see the layout of the 16 sonars. The code in the block below is similar to the code used in module 2. However, now we will use this code to make the system smart.\n",
    "<img src=\"pioneer_sonars.png\" alt=\"Pioneer Robot Sonar Locations\" width=\"200\"/>\n",
    "*Figure 1: Sonar sensor locations on robot*\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "from jyro.simulator import *\n",
    "import numpy as np\n",
    "\n",
    "#First, we define our world, inserting our coordinate system. \n",
    "def make_world(physics):\n",
    "    physics.addBox(0, 0, 4, 4, fill=\"backgroundgreen\", wallcolor=\"gray\") \n",
    "    \n",
    "#Then, we define the robot and its location in the coordinate system.    \n",
    "def make_robot():\n",
    "    robot = Pioneer(\"Rob\", 2, 1, 0) #paremeters are name, x, y, heading (in radians)\n",
    "    robot.addDevice(Pioneer16Sonars())\n",
    "    return robot\n",
    "\n",
    "robot = make_robot()\n",
    "\n",
    "\n",
    "#Add robot behaviour here\n",
    "vsim = VSimulator(robot, make_world)\n",
    "\n",
    "for i in range(250):\n",
    "    sonardata = robot[\"sonar\"].getData() #First we obtain the latest sensordata\n",
    "    if sonardata[3] <= 1.0 or sonardata[4] <= 1.0: #This code is executed if one or both of the sonar sensors at the\n",
    "                                                   #front of the robot measure a distance less than 1.0 m\n",
    "        robot.move(0,math.pi/0.5)\n",
    "        vsim.step()\n",
    "    else: #The code below is executed if a wall is NOT detected.\n",
    "        robot.move(1,0)\n",
    "        vsim.step()\n",
    "    time.sleep(0.1)\n",
    "\n",
    "print(\"Rob's pose: \",robot.getPose())\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Remember that in Module 2 we talked about making a smart decision as to which direction Rob should turn when he is approaching a wall?\n",
    "Let us now try and build a smart turning system, which makes Rob turn to the left if a wall is closeby on his right side and which makes Rob turn right if a wall is closeby on his left side.\n",
    "\n",
    "We start simple by defining how we will decide which direction to turn. Our first design for Rob's intelligence will use the two sonar beams pointed forward and the two sonar beams pointing forward.\n",
    "\n",
    "In the next section, we will slowly build our code with the help of *dummy variables*. These dummy variables will function as a stand in for the acutal sensor data. Take some time to examine the example in the code block below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Dummy variables - used to temporarily send fake sensor signals to Rob's decision system\n",
    "\n",
    "#side sonars\n",
    "sonar0 = 1.5 #This represents the value of sonar scanner 0, mounted on the left side of Rob\n",
    "sonar7 = 2.5 #This represents the value of sonar scanner 7, mounted on the right side of Rob\n",
    "\n",
    "#front sonars\n",
    "sonar3 = 1.7 #This represents the value of sonar scanner 3, facing forward on the left side of Rob.\n",
    "sonar4 = 1.7 #This represents the value of sonar scanner 4, facing forward on the right side of Rob.\n",
    "\n",
    "#Intelligence\n",
    "#Step 1: Decide if Rob is close to a wall\n",
    "if sonar3 <= 1.0 or sonar4 <= 1.0: #Code is executed if sonar3 or sonar4 (front facing) are equal to or lower than 1.0\n",
    "    print(\"Rob is about to hit a wall\")\n",
    "\n",
    "else: #Code is executed if sonar3 or sonar4 are larger than 1.0\n",
    "    print(\"Rob is not about to hit a wall\")\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Using the code block above, we can build a control system for Rob without having access to a physical robot or even a simulation. By simply changing the fake sensor values we can test if the system makes the decision we want it to make. \n",
    "\n",
    "*Exercise:* Change one or more of the **dummy variables** in the code block above to make the system report that Rob is about to hit a wall.\n",
    "\n",
    "Did you manage to make the outcome of our decision system change?\n",
    "The next step is to make Rob choose which side to turn if he is actually approaching a wall. We will use the code from the previous code block and change it, as shown below.\n",
    "We will add a second step to our decision system, but only to the part of code that is executed if Rob is actually approaching a wall. The part that is executed if Rob is not approaching a wall we will not touch."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Dummy variables - used to temporarily send fake sensor signals to Rob's decision system\n",
    "\n",
    "#side sonars\n",
    "sonar0 = 1.5 #This represents the value of sonar scanner 0, mounted on the left side of Rob\n",
    "sonar7 = 2.5 #This represents the value of sonar scanner 7, mounted on the right side of Rob\n",
    "\n",
    "#front sonars\n",
    "sonar3 = 1.7 #This represents the value of sonar scanner 3, facing forward on the left side of Rob.\n",
    "sonar4 = 1.7 #This represents the value of sonar scanner 4, facing forward on the right side of Rob.\n",
    "\n",
    "#Intelligence\n",
    "#Step 1: Decide if Rob is close to a wall\n",
    "if sonar3 <= 1.0 or sonar4 <= 1.0: #Code is executed if sonar3 or sonar4 (front facing) are equal to or lower than 1.0\n",
    "    print(\"Rob is about to hit a wall\")\n",
    "    \n",
    "    #Step 2: Rob is about to hit a wall, which side should he turn?\n",
    "    if sonar0 >= sonar7: #Code is executed if sonar0 (left side) measures a larger distance to the wall than sonar7 (right side)\n",
    "        print(\"Sonar 0 measures a distance larger than or equal to sonar 7\")\n",
    "    else: #Code is executed if sonar0 (left side) measures a smaller distance to the wall than sonar7 (right side)\n",
    "        print(\"Sonar 0 measures a distance smaller than sonar 7\")\n",
    "\n",
    "else: #Code is executed if sonar3 or sonar4 are larger than 1.0\n",
    "    print(\"Rob is not about to hit a wall\")\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Initially, running the code block above will cause the system to report that Rob is not about to hit a wall. Again, we should change the *dummy variables* to make the system report Rob is about to hit a wall.\n",
    "Only then will our *Step 2* code be executed.\n",
    "\n",
    "*Exercise:* Change the dummy variables in the code block above in such a way that:\n",
    "* The system reports that Rob is about to hit a wall\n",
    "* The system reports that sonar0 measures a larger value than sonar 7\n",
    "\n",
    "Did you succeed in changing the dummy variables in such a way that the system reports the required text?\n",
    "\n",
    "Let us take a second to think about what we have achieved so far:\n",
    "\n",
    "* First, we have built a system that can detect when Rob is about to hit a wall.\n",
    "\n",
    "* Second, we have built a system which checks for available space on the left and right side of Rob. \n",
    "\n",
    "The next step is to think about the behaviour we want Rob to show in each situation. The most efficient way of developing behaviour for each *scenario* is to make a list:\n",
    "\n",
    "* What if Rob is NOT about to hit a wall?\n",
    "* What if Rob IS about to hit a wall AND sonar0 measures a larger distance than sonar7?\n",
    "* What if Rob IS about to hit a wall AND sonar0 measures a smaller distance than sonar7?\n",
    "\n",
    "Please take your time to think about the behaviour Rob should show in each situation. To help you, the code box below shows the code again, but this time with the three scenarios integrated in the code."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Dummy variables - used to temporarily send fake sensor signals to Rob's decision system\n",
    "\n",
    "#side sonars\n",
    "sonar0 = 1.5 #This represents the value of sonar scanner 0, mounted on the left side of Rob\n",
    "sonar7 = 2.5 #This represents the value of sonar scanner 7, mounted on the right side of Rob\n",
    "\n",
    "#front sonars\n",
    "sonar3 = 1.7 #This represents the value of sonar scanner 3, facing forward on the left side of Rob.\n",
    "sonar4 = 1.7 #This represents the value of sonar scanner 4, facing forward on the right side of Rob.\n",
    "\n",
    "#Intelligence\n",
    "#Step 1: Decide if Rob is close to a wall\n",
    "if sonar3 <= 1.0 or sonar4 <= 1.0: #Code is executed if sonar3 or sonar4 (front facing) are equal to or lower than 1.0  \n",
    "    #Step 2: Rob is about to hit a wall, which side should he turn?\n",
    "    if sonar0 >= sonar7: #Code is executed if sonar0 (left side) measures a larger distance to the wall than sonar7 (right side)\n",
    "        print(\"Rob is about to hit a wall AND sonar 0 measures a distance larger than or equal to sonar 7\")\n",
    "    else: #Code is executed if sonar0 (left side) measures a smaller distance to the wall than sonar7 (right side)\n",
    "        print(\"Rob is about to hit a wall AND sonar 0 measures a distance smaller than sonar 7\")\n",
    "\n",
    "else: #Code is executed if sonar3 or sonar4 are larger than 1.0\n",
    "    print(\"Rob is NOT about to hit a wall\")\n",
    "    "
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Have you decided for yourself which behaviour Rob should show for each scenario?\n",
    "Your answers should be similar to the answers below:\n",
    "\n",
    "If Rob is not about to hit a wall:\n",
    "Rob should continue driving forward\n",
    "\n",
    "If Rob is about to hit a wall and sonar 0 measures a distance larger than sonar 7: Rob should make a turn to the left, as there is more space on the left than on the right.\n",
    "\n",
    "If Rob is about to hit a wall and sonar 0 measures a distance smaller than sonar 7: Rob should make a turn to the right, as there is more space on the right than on the left.\n",
    "\n",
    "The code block below contains our decision system, which is now printing Rob's behaviour. Use the dummy variables to change the output of our decision system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "#Dummy variables - used to temporarily send fake sensor signals to Rob's decision system\n",
    "\n",
    "#side sonars\n",
    "sonar0 = 1.5 #This represents the value of sonar scanner 0, mounted on the left side of Rob\n",
    "sonar7 = 2.5 #This represents the value of sonar scanner 7, mounted on the right side of Rob\n",
    "\n",
    "#front sonars\n",
    "sonar3 = 1.7 #This represents the value of sonar scanner 3, facing forward on the left side of Rob.\n",
    "sonar4 = 1.7 #This represents the value of sonar scanner 4, facing forward on the right side of Rob.\n",
    "\n",
    "#Intelligence\n",
    "#Step 1: Decide if Rob is close to a wall\n",
    "if sonar3 <= 1.0 or sonar4 <= 1.0: #Code is executed if sonar3 or sonar4 (front facing) are equal to or lower than 1.0  \n",
    "    #Step 2: Rob is about to hit a wall, which side should he turn?\n",
    "    if sonar0 >= sonar7: #Code is executed if sonar0 (left side) measures a larger distance to the wall than sonar7 (right side)\n",
    "        print(\"Rob should turn left\")\n",
    "    else: #Code is executed if sonar0 (left side) measures a smaller distance to the wall than sonar7 (right side)\n",
    "        print(\"Rob should turn right\")\n",
    "\n",
    "else: #Code is executed if sonar3 or sonar4 are larger than 1.0\n",
    "    print(\"Rob should continue driving forward\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Now we have finished our decision system which will make Rob fully autonomous. Time to put this system in Rob's brain. This means we will have to replace the dummy variables with the actual sensor values and we will have to replace the commands to print text by actual commands to Rob.\n",
    "\n",
    "We will do this by combining code from the code block above and the first code block in this lesson. Take some time to recognise elements from each piece of code in the code block below."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vsim = VSimulator(robot, make_world)\n",
    "\n",
    "for i in range(250):\n",
    "    sonardata = robot[\"sonar\"].getData() #First we obtain the latest sensordata\n",
    "    \n",
    "    #Intelligence\n",
    "    #Step 1: Decide if Rob is close to a wall\n",
    "    if sonardata[3] <= 1.0 or sonardata[4] <= 1.0: #Code is executed if sonar3 or sonar4 (front facing) are equal to or lower than 1.0  \n",
    "        #Step 2: Rob is about to hit a wall, which side should he turn?\n",
    "        if sonardata[0] >= sonardata[7]: #Code is executed if sonar0 (left side) measures a larger distance to the wall than sonar7 (right side)\n",
    "            robot.move(0,math.pi/0.5) #Makes Rob turn left\n",
    "            vsim.step()\n",
    "        else: #Code is executed if sonar0 (left side) measures a smaller distance to the wall than sonar7 (right side)\n",
    "            robot.move(0,-1*math.pi/0.5) #Makes Rob turn right (notice the -1)\n",
    "            vsim.step()\n",
    "\n",
    "    else: #Code is executed if sonar3 or sonar4 are larger than 1.0\n",
    "        robot.move(1,0)\n",
    "        vsim.step()\n",
    "    \n",
    "    \n",
    "    time.sleep(0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Did the code above work? Congratulations, you have programmed your own smart robot! \n",
    "\n",
    "## Driving past a wall at a fixed distance\n",
    "\n",
    "So far we have tried to stay away from walls. So what will happen if we wish to stay at a fixed distance from an obstacle. This can happen for example if you are working on the field and you wish to plant your first row of crops at exactly 0.25 m from the side of the field.\n",
    "\n",
    "In this experiment, we will place a block in the middle of Rob's room. Rob should drive around this block with a distance of 0.5 m from the block as accurately as possible. execute the code block below to see the situation visualized."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def make_world(physics):\n",
    "    physics.addBox(0, 0, 4, 4, fill=\"backgroundgreen\", wallcolor=\"gray\") \n",
    "    physics.addBox(1.5,2.75,2.5,2.25)\n",
    "vsim = VSimulator(robot, make_world)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "We should now program Rob in such a way that he detects the block and starts driving around it. \n",
    "Rob's intelligence should now be able to recognise two situations:\n",
    "* Driving towards the block\n",
    "* Driving around the block\n",
    "\n",
    "Each situation requires Rob to show different behaviour, which is why we need to program Rob not for 1 but for 2 situations. We call these situations *states*, and Rob's behaviour should change according to his state.\n",
    "\n",
    "In his first state, *driving towards the block*, Rob should drive forward until his front sensors indicate he is at the required distance from the block. Only when this *requirement* is met can Rob stop searching for the block and start driving around the block. So the *driving towards the block* state should have a trigger telling Rob to switch to the second state, which is *driving around the block*.\n",
    "\n",
    "Check out the code below to see how it works.\n",
    "\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vsim = VSimulator(robot, make_world)\n",
    "\n",
    "state = \"DriveTowardsBlock\" #Define that Rob should start by searching for the block\n",
    "\n",
    "\n",
    "for i in range(250): #Our simulation runs for 25 seconds\n",
    "    sonardata = robot[\"sonar\"].getData() #First we obtain the latest sensordata, this is needed for both states\n",
    "    \n",
    "    if state == \"DriveTowardsBlock\":\n",
    "        robot.move(1,0) #Drive straight forward\n",
    "        \n",
    "        if sonardata[3]<= 0.5 or sonardata[4] <= 0.5: #Requirement to switch to different state\n",
    "            state = \"DriveAroundBlock\"\n",
    "            print(\"Switching to DriveAroundBlock state\")\n",
    "        \n",
    "    elif state == \"DriveAroundBlock\":\n",
    "        print(\"Rob now driving around block\")\n",
    "        break\n",
    "    vsim.step()\n",
    "    time.sleep(0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As we can see, Rob is now driving towards the block for 0.7 seconds (notice the time counter in the right top corner of the simulation window). Then our *condition* for Rob to switch to his second state is met. As we have not programmed the second state yet, Rob now simply stops driving. This is the next thing we should do"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "vsim = VSimulator(robot, make_world)\n",
    "\n",
    "state = \"DriveTowardsBlock\" #Define that Rob should start by searching for the block\n",
    "\n",
    "\n",
    "for i in range(250): #Our simulation runs for 25 seconds\n",
    "    sonardata = robot[\"sonar\"].getData() #First we obtain the latest sensordata, this is needed for both states\n",
    "    \n",
    "    if state == \"DriveTowardsBlock\":\n",
    "        robot.move(1,0) #Drive straight forward\n",
    "        \n",
    "        if sonardata[3]<= 0.5 or sonardata[4] <= 0.5: #Requirement to switch to different state\n",
    "            robot.move(0,math.pi/0.2) #Prepare Rob for next state, make 90 degrees turn.\n",
    "            state = \"DriveAroundBlock\"\n",
    "            print(\"Switching to DriveAroundBlock state\")\n",
    "        \n",
    "    elif state == \"DriveAroundBlock\":\n",
    "        if sonardata[7] <= 0.5:\n",
    "            robot.move(0.5,math.pi/4)\n",
    "        else:\n",
    "            robot.move(0.5,-math.pi/4)\n",
    "        \n",
    "        \n",
    "            \n",
    "    vsim.step()\n",
    "    time.sleep(0.1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "As you can see, Rob is nicely driving around the block.\n",
    "We can see Rob detecting the block and switching to the `DriveAroundBlock` state nicely. He then drives past the block and starts turning upon reaching the ends.\n",
    "However, there is one small problem. Imagine if Rob was a tractor seeding crops around this block. Rerun the code block above, and carefully watch what happens if Rob drives from left to right and from right to left.\n",
    "\n",
    "Can you see Rob shaking? Even though our control system works, operations are not yet so smooth. This is something we will work on in the next module.\n",
    "\n",
    "For now, congratulations on building your first autonomous robot and finishing this module!"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.6"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 2
}
